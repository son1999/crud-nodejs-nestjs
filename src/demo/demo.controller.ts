import { Controller, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { DemoService } from './demo.service';
import { DemoDTO } from './demo.dto';

@Controller('demo')
export class DemoController {

    constructor(private demoService: DemoService) { }

    @Get()
    showAll() {
        return this.demoService.showAll();
    }

    @Post()
    createDemo(@Body() data: DemoDTO) {
        return this.demoService.create(data);
    }

    @Get(':id')
    readDemo(@Param('id') id: string) {
        return this.demoService.read(id);
    }

    @Put(':id')
    updateDemo(@Param('id') id: string, @Body() data: Partial<DemoDTO>) {
        return this.demoService.update(id, data);
    }

    @Delete(':id')
    destroyDemo(@Param('id') id: string) {
        return this.demoService.destroy(id);
    }
}
