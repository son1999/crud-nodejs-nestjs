import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { DemoEntity } from './demo.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DemoDTO } from './demo.dto';

@Injectable()
export class DemoService {
    constructor(@InjectRepository(DemoEntity)
    private demoRepository: Repository<DemoEntity>
    ) { }

    async showAll() {
        return await this.demoRepository.find();
    }

    async create(data: DemoDTO) {
        const demo = await this.demoRepository.create(data);
        await this.demoRepository.save(data);
        return data;
    }

    async read(id: string) {
        return await this.demoRepository.findOne({ where: { id } });
    }

    async update(id: string, data: Partial<DemoDTO>) {
        await this.demoRepository.update({ id }, data);
        return await this.demoRepository.findOne({ id });
    }

    async destroy(id: string) {
        await this.demoRepository.delete({ id });
        return await this.demoRepository.findOne({ id });
    }
}
